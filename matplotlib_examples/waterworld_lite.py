#!/usr/bin/env python3

'''this is a slimmed-down version of a one-script solution of the Assignment 
2016, adopted for the visualisation lecture in COMP7230.2017 as an example'''

# coding: utf-8

import sys
import argparse
import numpy as np
import pylab as pl
from matplotlib.colors import LightSource

# parser = argparse.ArgumentParser()
# parser.add_argument("data_file_name", help="requires a name of data file")
# parser.parse_args()

try:
	datafile = sys.argv[1]
	water_level = 0 if len(sys.argv) == 2 else int(sys.argv[2])
except IndexError as ie:
	print("Usage: python3 waterworld.py fname [sea_level]")
	sys.exit(1)
# TODO Calculate the number of islands
# TODO Clean-up the colobar in the terrain plot
# TODO Add argparse usage to process CL args
# TODO Use memmap and time the computation for large (0.5Gb) data sets
# TODO Create a submerging movie a la "Waterworld" (Universal Pictures, 1995)

ECIRCLE = 40075 # equatorial circle
POLCIRCLE = 40007 # polar circle
VERTICAL_RESOLUTION = 100 # number of iterations when flooding

# Using the structured array to contain all -- geodesic coordinates and heights
dt = np.dtype([('y', np.float), ('x', np.float), ('height', np.float)])

# Read in the data (later, we should consider memmap approach!)
try:
	data = np.genfromtxt(datafile,
	dtype=dt,
	delimiter=' ',
	autostrip=True,
	invalid_raise=True)
except IOError as ioe:
	print(ioe)
	print("Info: the data file are located in ./data directory\n",
		"(it can be read from gzip-compressed file directly)")
	sys.exit(1)

# Calculate the longitudinal and latitudinal spans of the data set
y0 = data[0][0]
N = 0
for d in data:
    if not d[0] == y0:
        break
    else:
        N += 1

M = data.size // N

# Reshaping the array
data = data.reshape(M,N)

# Extract the longitudinal ('x') and and latitudinal ('y') spans,
# assuming that they are homogeneous (constant over the resiprocal coordinate)!
xs = data['x'][0]
ys = data['y'][:,0]

# Calculating the size of intervals (they may not be constant!)
dxs = xs[1:] - xs[:-1]
dys = ys[:-1] - ys[1:]

# Calculating the segment areas
areas = (ECIRCLE*POLCIRCLE/(360*360) *
		(dxs * np.cos(np.radians(ys[:-1, None])) * dys[:, np.newaxis]))

# Extracting the heights
heights = data['height']

# Calculating the mean values for x- and y-size of the data segments
delta_y_average = np.average(dys * POLCIRCLE / 360)
delta_x_spreads = np.average(dxs) *((ECIRCLE / 360) * np.cos(np.radians(ys)))
delta_x_average = np.average(delta_x_spreads)

# Reporting the mean values
print("Average spacings: latitude {:.3f} and longitude {:.3f}".
			format(delta_y_average, delta_x_average))

# Calculating the total area of dry land (above the sea level excluding the
# land in depressions which is locked inside the dry land)

tallest_point = np.max(heights)
zero_level_area = np.sum(areas)

# curtailing the height map
heights = heights[:-1,:-1]

# initialise array to hole areas above sea level up to complete submerging
area_at_sea_level = np.zeros(VERTICAL_RESOLUTION)
height_increment = tallest_point/VERTICAL_RESOLUTION

# fill with the dry land area values
for h in range(VERTICAL_RESOLUTION):
	area = np.sum(areas[heights > h*height_increment])
	area_at_sea_level[h] = area

print("The highest point is {:.2f}".format(tallest_point))

# Plotting the data: the map of land surrounded by sea

fig, ax = pl.subplots(ncols=1, figsize=(6.5, 5))

# Plotting the "map" -- land surrounded by [risen] seas
heights[heights < water_level] = 0

ls = LightSource(azdeg=xs[0], altdeg=ys[0])

# cmap = pl.cm.jet # default colormap
cmap = pl.cm.terrain # another option for colormap
# cmap = pl.cm.gist_earth # yet another

rgb = ls.shade(heights, cmap=cmap, blend_mode='overlay')#, dx=xs[0], dy=ys[0])
# rgb = ls.shade(heights, cmap=pl.cm.bone, blend_mode='overlay')
# pl.colorbar(ax1.imshow)

ax.set_xticks(range(0,N,int(N/5)))
ax.set_xticklabels(['{:.2f}\u00B0'.format(v) 
			for v in xs[::int(N/5)]], size='small')
ax.set_yticks(range(0,M,int(M/5)))
ax.set_yticklabels(['{:.2f}\u00B0'.format(v) 
			for v in ys[::int(M/5)]], size='small')
ax.set_xlabel(r'$\Theta$')
ax.set_ylabel(r'$\Phi$')

image = ax.imshow(rgb)

# add a colourbar
fig.colorbar(image, ax=ax, shrink=0.9)

ax.set_title('The dry land when the sea level has risen {:d} meters'
				.format(water_level), size='medium')

fig.subplots_adjust(bottom=0.2, right=0.95)
pl.show()
