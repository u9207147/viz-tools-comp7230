import pylab as pl
import numpy as np
import math
from scipy.integrate import quad
from scipy.special import j0

# pl.ion()

def j0i(x):
    """Integral to get Bessel J_0(x)"""
    def integrand(phi):
        return math.cos(x*math.sin(phi))
    return (1.0/math.pi)*quad(integrand, 0, math.pi)[0]


x = np.linspace(0,20,200) # grid 200 points between 0..20
y = j0(x) # sample J_0 at ALL values of x
x1 = x[::10] # subsample of the original grid x every 10th point
y1 = list(map(j0i, x1)) # evaluate integral from all points in x1

# make plot (without displaying) with the above values 

pl.plot(x,y,label=r"$J_{0}(x)$"); # semicolon supresses display
pl.plot(x1, y1, 'ro', label=r'$J_0^{\mathrm{integ}}(x)$');
pl.axhline(0,color='green',label='_nolegend_');
pl.title(r'Verify $J_0(x)=\frac{1}{\pi}\,\int_0^{\pi}\cos(x\,\sin\phi)\,d\phi$',
		verticalalignment='bottom');
pl.xlabel('$x$');
pl.ylabel("Bessel function $J_0(x)$")
pl.legend();
# pl.ioff()
pl.show()
